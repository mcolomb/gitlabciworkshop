# Modify the `.gitlab-ci.yml` to build and deploy documentation using GitLab Pages

*Please verify that GitLab Pages [is activated](2.setup_ci.md#enable-gitlab-pages) and has desired visibility level.*

## 1st version - Create pages job

- Add a new job section in the `.gitlab-ci.yml` file to publish the static html files of
your project. For GitLab Pages, this jobs has a specific name:
**[pages](https://docs.gitlab.com/ee/ci/yaml/#pages)**.

- Set the [stage](https://docs.gitlab.com/ee/ci/yaml/#stage) of the job as
  `deploy`. The jobs at `deploy` stage will run after the jobs  in `.pre`,
  `build` and `test` stages are executed.

- Modify [script](https://docs.gitlab.com/ee/ci/yaml/#script) section to add
 the required steps to [build documentation](../README.md#build-documentation).


```yml
pages:
  image: python:3.12
  stage: deploy
  tags:
    - ci.inria.fr # For using shared runners
    - small       # For selecting small shared runners
  script:
    (...)  
```
- Add the `.gitlab-ci.yml` file, commit and push your modifications.

- Verify the CI is launched and successful.

- Verify a new job `pages:deploy` is added to the pipeline and fails.

> Whenever there is a job called **pages**, GitLab Pages creates an additional
job `pages:deploy`. This job requires a directory called **public** provided as
artifact that contains all files to be deployed. As we haven't provided this
directory as artifact, `pages:deploy` job has failed.

## 2nd version - Add public directory to artifacts

- In the `script` section, copy generated html files under `public` directory
  in the project root directory.
  
- Modify **[artifacts](https://docs.gitlab.com/ee/ci/yaml/#artifacts)** section
  to share `public` directory as artifact.

```yml
  script:
    (...)
    - mv _build/html ../public/
  artifacts:
    paths:
      - public
```

- Add the `.gitlab-ci.yml` file, commit and push your modifications.

- Verify the CI is launched and successful.

- Verify that the html pages are published.
    - On the left sidebar in the GitLab interface, go to **Deploy** → **Pages**.
    - Click on link provided in the **Access Pages** section.

## 3rd version - Build documentation only for default branch (OPTIONAL)

- Add [rules](https://docs.gitlab.com/ee/ci/yaml/#rules) section in the
`.gitlab-ci.yml` file to run `pages` job only for pushes to the default branch
using [Predefined GitLab CI variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) `CI_COMMI_REF_NAME` and `CI_DEFAULT_BRANCH`. 

| Variable | Description |
| -------- | ----------- |
| CI_COMMIT_REF_NAME | The branch or tag name for which project is built. |
| CI_DEFAULT_BRANCH | The name of the project’s default branch. |

```yml

  script:
    (...)
  artifacts:
    (...)
  rules:
    # Ensures that only pushes to the default branch will trigger
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
 ```

- Add the `.gitlab-ci.yml` file, commit and push your modifications.

- Verify the CI is launched and successful.

- Verify that the html pages are published.


***

[go to next](5.yml_for_docker.md)
