# Modify the `.gitlab-ci.yml` to build a Docker image and push to GitLab Registry

*Please verify that GitLab Container Registry [is activated](2.setup_ci.md#enable-container-registry) and has desired visibility level.*

## 1st version - Create a job to build Docker image

- Create a new job with name `build docker` using [image](https://docs.gitlab.com/ee/ci/yaml/#image) `docker:20.10.16`.

- Set the `stage` of the job as `.pre`. It will run before all defined stages in the `.gilab-ci.yml` file.

- Modify `script` section to add the steps to [build the Docker image](../docker/README.md#build-docker-image).

```yml
build docker:
  image: docker:20.10.16
  stage: .pre
  tags:
    - ci.inria.fr # For using shared runners
    - small       # For selecting small shared runners
  script:
    (...)
```

- Verify the CI is launched and successful.

## 2nd version - Push Docker Image to GitLab Container Registry

- Add [before_script](https://docs.gitlab.com/ee/ci/yaml/#before_script)
  section to [Authenticate with the GitLab container registry](https://docs.gitlab.com/ee/user/packages/container_registry/authenticate_with_container_registry.html) using [Predefined GitLab CI variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
`CI_REGISTRY_USER`, `CI_REGISTRY_PASSWORD` and `CI_REGISTRY`.

| Variable | Description |
| -------- | ----------- |
| CI_REGISTRY_USER | The username to push containers to the project’s GitLab container registry. Only available if the container registry is enabled for the project. Its value is `gitlab-ci-token`. |
| CI_REGISTRY_PASSWORD | The password to push containers to the GitLab project’s container registry. Only available if the container registry is enabled for the project. This password value is the same as the CI_JOB_TOKEN and is valid only as long as the job is running. Use the CI_DEPLOY_PASSWORD for long-lived access to the registry. |
| CI_REGISTRY | Address of the container registry server, formatted as <host>[:<port>]. For example: registry.gitlab.example.com. Only available if the container registry is enabled for the GitLab instance. |

```yml
  tags:
    (...)
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    (...)
```

- Define `IMAGE_TAG` as a variable using
[variables](https://docs.gitlab.com/ee/ci/yaml/#variables) section and Predefined GitLab CI variables `CI_REGISTRY_IMAGE` and `CI_COMMIT_REF_SLUG`.
> To be able to push to GitLab registry, image names should follow the container
registry naming convention. This name is provided by `CI_REGISTRY_IMAGE`
variable. Instead of tagging the image with `v1`, we can use
`CI_COMMIT_REF_SLUG` and tag using the branch name.

| Variable | Description |
| -------- | ----------- |
| CI_REGISTRY_IMAGE | Base address for the container registry to push, pull, or tag project’s images, formatted as `<host>[:<port>]/<project_full_path>`. For example: `registry.gitlab.example.com/my_group/my_project`. Image names must follow the container registry naming convention. Only available if the container registry is enabled for the project. |
| CI_COMMIT_REF_SLUG | `CI_COMMIT_REF_NAME` in lowercase, shortened to 63 bytes, and with everything except 0-9 and a-z replaced with -. No leading / trailing -. Use in URLs, host names and domain names.  |

```yml
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    (...)

```


- Modify `script` section to build Docker image using `IMAGE_TAG` variable.

- Modify `script` section to push generated Docker image to GitLab container registry.

```yml

  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    # build Docker image
    - docker build -t $IMAGE_TAG -f docker/Dockerfile .
    # push image to registry
    - docker push $IMAGE_TAG
```

- Verify the CI is launched and successful.

- Verify that the Docker image is saved in the GitLab container registry.
    - On the left sidebar in the GitLab interface, go to **Deploy** → **Container Registry**.

## 3rd version - Use Docker Image for CI Jobs

- Move `variables` section to global level.

- Update `image` section of job `test python 3.12` with `$IMAGE_TAG`.

- Update `image` section of job `pages` with `$IMAGE_TAG`.

```yml
variables:
  IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG

build docker:
  image: docker:20.10.16
  stage: .pre
  tags:
    - ci.inria.fr # For using shared runners
    - small       # For selecting small shared runners
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    # build Docker image
    - docker build -t $IMAGE_TAG -f docker/Dockerfile .
    # push image to registry
    - docker push $IMAGE_TAG

test python 3.12:    
  image: $IMAGE_TAG
  stage: test
  tags:
    - ci.inria.fr
    - small
  script:
    - pip install -e .
    - pytest --cov=projection --cov-report term-missing --cov-report xml:coverage.xml --cov-report html
  coverage: '/^TOTAL.+?(\d+\%)$/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
    paths:
      - htmlcov
      - coverage.xml  

test python 3.11:    
  image: python:3.11 
  stage: test        
  tags:
    - ci.inria.fr
    - small
  script:
    - pip install -e .[test]
    - pytest

pages:
  image: $IMAGE_TAG
  stage: deploy
  tags:
    - ci.inria.fr # For using shared runners
    - small       # For selecting small shared runners
  script:
    - pip install -e .
    - cd doc
    - make html
    - mv _build/html ../public/
  artifacts:
    paths:
      - public
  rules:
    # Ensures that only pushes to the default branch will trigger
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

- Verify the CI is launched and successful.
